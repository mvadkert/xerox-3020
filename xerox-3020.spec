%global __brp_check_rpaths %{nil}

Summary: Driver for Xerox 3020
Name: xerox-3020
Version: 1.0.0
Release: 1
License: GPLv2
URL: https://www.support.xerox.com/en-us/product/phaser-3020

Source0: https://download.support.xerox.com/pub/drivers/3020/drivers/linux/en_GB/Xerox_Phaser_3020_LinuxDriver.tar.gz

Requires: cups

%description
This driver is usable by all printer devices which understand the QPDL
(Quick Page Description Language) also known as SPL2 (Samsung Printer Language)
language. It covers several Samsung, Xerox and Dell printers.
Splix doesn't support old SPL(1) printers.

%prep
%setup -q -n uld

%install
mkdir -p %{buildroot}/usr/lib/cups/backend
mkdir -p %{buildroot}/usr/lib/cups/filter
mkdir -p %{buildroot}/usr/share/ppd/xerox
mkdir -p %{buildroot}/opt/smfp-common/printer/bin
mkdir -p %{buildroot}/opt/smfp-common/printer/lib 
cp x86_64/pstosecps %{buildroot}/usr/lib/cups/filter/pstosecps
cp x86_64/rastertospl %{buildroot}/usr/lib/cups/filter/rastertospl
cp x86_64/libscmssc.so %{buildroot}/opt/smfp-common/printer/lib/libscmssc.so
cp x86_64/smfpnetdiscovery %{buildroot}/opt/smfp-common/printer/bin/smfpnetdiscovery
cp x86_64/smfpnetdiscovery %{buildroot}/usr/lib/cups/backend/smfpnetdiscovery
cp noarch/share/ppd/Xerox_Phaser_3020.ppd %{buildroot}/usr/share/ppd/xerox/Xerox_Phaser_3020.ppd

%files
/usr/lib/cups/filter/pstosecps
/usr/lib/cups/filter/rastertospl
/usr/lib/cups/backend/smfpnetdiscovery
/usr/share/ppd/xerox/Xerox_Phaser_3020.ppd
/opt/smfp-common/printer/lib/libscmssc.so
/opt/smfp-common/printer/bin/smfpnetdiscovery

%changelog
* Sun Apr 02 2023 Miroslav Vadkerti <mvadkert@redhat.com> - 1.0.0-1
- Initial spec file.
